import matplotlib
import matplotlib.pyplot as plt

#tiempo
t0 = 0
tmax = 20
step = 100000
n_muestra = int((tmax - t0)*step)

#variables de estado
s = 762
i = 1
r = 0

#datos
beta = 0.00218
gamma = 0.4404

#historial de estados
ss = []
ii = []
rr = []

ss.append(s) 
ii.append(i) 
rr.append(r) 
ristra = range(n_muestra)

for ciclo in ristra:
    stemp = s
    itemp = i
    rtemp = r
    s = ss[-1] - beta*ii[-1]*ss[-1]/step
    i = ii[-1] + (beta*ii[-1]*ss[-1] -gamma*ii[-1])/step
    r = rr[-1] + gamma*ii[-1]/step
    ss.append(s) 
    ii.append(i) 
    rr.append(r) 

#Infectados por dia
iday = []
for i in range(tmax):
    iday.append(round(ii[i*step]))

print(iday)
    
#print
titulo = 'Paso de ' + str(1/step)
plt.plot(ristra, ss[0:-1], ristra, ii[0:-1], ristra, rr[0:-1])
plt.title(titulo)
plt.show()
